package com.bat.weatherapp.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bat.weatherapp.R;
import com.bat.weatherapp.adapters.DayAdapter;
import com.bat.weatherapp.weather.Day;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class DailyForecastActivity extends Activity {

    private Day[] mDays;

    @InjectView(android.R.id.list) ListView mListView;
    //@InjectView(android.R.id.empty) TextView emptyView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_forecast);
        ButterKnife.inject(this);


        RelativeLayout layout = (RelativeLayout) findViewById(R.id.dailyLayout);
        TextView location = (TextView) findViewById(R.id.locationLabel);

        Intent intent = getIntent();
        Parcelable[] parcelables = intent.getParcelableArrayExtra(MainActivity.DAILY_FORECAST);
        mDays = Arrays.copyOfRange(parcelables, 0, 8, Day[].class);


        layout.setBackground(MainActivity.BACKGROUND_GRADIENT);
        location.setText(mDays[0].getTimezone());

        DayAdapter adapter = new DayAdapter(this, mDays);
        mListView.setAdapter(adapter);
        //mListView.setEmptyView(emptyView);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String dayOfTheWeek = mDays[position].getDayOfTheWeek();
                String highTemp = mDays[position].getTemperatureMax() + "";
                if(dayOfTheWeek.equals("კვირა")){
                    dayOfTheWeek = "კვირას";
                }else{
                    dayOfTheWeek = dayOfTheWeek.substring(0, dayOfTheWeek.length()-1);
                }
                String message = String.format("%sს დღისით მაქსიმალური ტემპერატურა იქნება %s", dayOfTheWeek, highTemp);

                Toast.makeText(DailyForecastActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });


    }

}
