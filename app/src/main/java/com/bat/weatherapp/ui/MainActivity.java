package com.bat.weatherapp.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bat.weatherapp.R;
import com.bat.weatherapp.weather.Current;
import com.bat.weatherapp.weather.Day;
import com.bat.weatherapp.weather.Forecast;
import com.bat.weatherapp.weather.Hour;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class MainActivity extends Activity {

    public static GradientDrawable BACKGROUND_GRADIENT =
            new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,null);
    public static final String TAG = MainActivity.class.getSimpleName();

    public static final String HOURLY_FORECAST = "hourly_forecast";
    public static final String DAILY_FORECAST = "daily_forecast";

    private static final String TOP_COLOR_KEY = "top_color_key";
    private static final String BOTTOM_COLOR_KEY = "bottom_color_key";
    private static final String PREFS_FILE = "com.bat.weatherapp.preferences";

    private Forecast mForecast;
    private SharedPreferences.Editor mEditor;
    private SharedPreferences mSharedPreferences;

    private int topBackgroundColor, bottomBackgroundColor;

    @InjectView(R.id.timeLabel)
    TextView mTimeLabel;
    @InjectView(R.id.temperatureLabel)
    TextView mTemperatureLabel;
    @InjectView(R.id.locationLabel)
    TextView mLocationLable;
    @InjectView(R.id.humidityValue)
    TextView mHumidityValue;
    @InjectView(R.id.precipValue)
    TextView mPrecipValue;
    @InjectView(R.id.summaryLable)
    TextView mSummaryLable;
    @InjectView(R.id.iconImageView)
    ImageView mIconImageView;
    @InjectView(R.id.refreshView)
    ImageView mRefreshView;
    @InjectView(R.id.progressBar)
    ProgressBar mProgressBar;
    @InjectView(R.id.relativeLayout)
    RelativeLayout mBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        mTemperatureLabel = (TextView) findViewById(R.id.temperatureLabel);
        mProgressBar.setVisibility(View.INVISIBLE);
        mSharedPreferences = getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();


        topBackgroundColor = mSharedPreferences.getInt(TOP_COLOR_KEY, 0);
        bottomBackgroundColor = mSharedPreferences.getInt(BOTTOM_COLOR_KEY, 0);

        if(topBackgroundColor == 0){
            topBackgroundColor = Color.parseColor("#FC970B");
        }
        if(bottomBackgroundColor == 2131492937){
            bottomBackgroundColor = Color.parseColor("#f25019");
        }

        BACKGROUND_GRADIENT.setColors(new int[]{topBackgroundColor, bottomBackgroundColor});
        mBackground.setBackground(BACKGROUND_GRADIENT);

        mRefreshView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getForecast();
            }
        });

        getForecast();

    }



    @Override
    public void onPause() {
        super.onPause();
        mEditor.putInt(TOP_COLOR_KEY, topBackgroundColor);
        mEditor.putInt(BOTTOM_COLOR_KEY, bottomBackgroundColor);

        if(mEditor.commit())
            mEditor.apply();
    }


    private void getForecast() {
        String API = "58e3aeb60f39a471f040b8ac47505079";
        double latitude = 41.6947;
        double longitude = 44.8749;
        String forecastUrl = "https://api.forecast.io/forecast/" + API + "/" + latitude + "," + longitude;


        if (isNetworkAvailable()) {
            toggleRefresh();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(forecastUrl).build();

            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toggleRefresh();
                        }
                    });
                    alertUserAboutError();
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toggleRefresh();
                        }
                    });

                    try {
                        String jsonData = response.body().string();
                        //Log.v(TAG, jsonData);
                        if (response.isSuccessful()) {
                            mForecast = parseForecastDetails(jsonData);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateDisplay();
                                }
                            });

                        } else {
                            alertUserAboutError();
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Exception caught:", e);
                    } catch (JSONException e) {
                        Log.e(TAG, "Exception caught:", e);
                    }

                }
            });
        } else {
            Toast.makeText(this, R.string.network_unavailable, Toast.LENGTH_LONG).show();
        }
    }

    private void toggleRefresh() {
        if (mProgressBar.getVisibility() == View.INVISIBLE) {
            mProgressBar.setVisibility(View.VISIBLE);
            mRefreshView.setVisibility(View.INVISIBLE);
        } else {

            mProgressBar.setVisibility(View.INVISIBLE);
            mRefreshView.setVisibility(View.VISIBLE);
        }
    }

    private void updateDisplay() {
        if(isNetworkAvailable()) {
            Current mCurrent = mForecast.getCurrent();

            bottomBackgroundColor = mCurrent.getMoodColor();

            mTemperatureLabel.setText(mCurrent.getTemperature() + "");
            mTimeLabel.setText("ამჟამად " + mCurrent.getFormattedTime() + " ზე არის");
            mHumidityValue.setText(mCurrent.getHumidity() + "");
            mPrecipValue.setText(mCurrent.getPrecipChance() + "%");
            mSummaryLable.setText(mCurrent.getSummary());
            mLocationLable.setText(mCurrent.getTimeZone());

            mIconImageView.setImageResource(mCurrent.getIconId());
        }
    }


    private Forecast parseForecastDetails(String jsonData) throws JSONException{
        Forecast forecast = new Forecast();

        forecast.setCurrent(getCurrentDetails(jsonData));
        forecast.setHourlyForecast(getHourlyForecast(jsonData));
        forecast.setDailyForecast(getDailyForecast(jsonData));



        return forecast;
    }

    private Day[] getDailyForecast(String jsonData) throws JSONException{
        JSONObject forecast = new JSONObject(jsonData);
        String timezone = forecast.getString("timezone");

        JSONObject dayly = forecast.getJSONObject("daily");

        JSONArray data = dayly.getJSONArray("data");

        Day[] days = new Day[data.length()];

        for(int i=0; i<days.length; i++){
            JSONObject jDay = data.getJSONObject(i);
            Day day = new Day();

            day.setSummary(jDay.getString("summary"));
            day.setTime(jDay.getLong("time"));
            day.setIcon(jDay.getString("icon"));
            day.setTemperatureMax(jDay.getDouble("temperatureMax"));
            day.setTimezone(timezone);

            days[i] = day;
        }

        return days;

    }

    private Hour[] getHourlyForecast(String jsonData) throws  JSONException{

        JSONObject forecast = new JSONObject(jsonData);
        String timezone = forecast.getString("timezone");

        JSONObject hourly = forecast.getJSONObject("hourly");

        JSONArray data = hourly.getJSONArray("data");

        Hour[] hours = new Hour[data.length()];

        for (int i=0; i<hours.length; i++){
            JSONObject jHour = data.getJSONObject(i);
            Hour hour = new Hour();

            hour.setSummary(jHour.getString("summary"));
            hour.setTime(jHour.getLong("time"));
            hour.setIcon(jHour.getString("icon"));
            hour.setTemperature(jHour.getDouble("temperature"));
            hour.setTimeZone(timezone);

            hours[i] = hour;

        }

        return hours;


    }

    private Current getCurrentDetails(String jsonData) throws JSONException {

        JSONObject forecast = new JSONObject(jsonData);
        String timezone = forecast.getString("timezone");

        JSONObject currently = forecast.getJSONObject("currently");

        Current current = new Current();
        current.setHumidity(currently.getDouble("humidity"));
        current.setTime(currently.getLong("time"));
        current.setIcon(currently.getString("icon"));
        current.setPrecipChance(currently.getDouble("precipProbability"));
        current.setSummary(currently.getString("summary"));
        current.setTemperature(currently.getDouble("temperature"));
        current.setTimeZone(timezone);



        return current;

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;

        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }


        return isAvailable;
    }

    private void alertUserAboutError() {
        AlertDialogFragment dialog = new AlertDialogFragment();
        dialog.show(getFragmentManager(), "error_dialog");
    }


    public void changeColor(View view) {


        ColorPickerDialogBuilder
                .with(this)
                .setTitle("აირჩიე ფერი")
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(8)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {
                        //just nothing
                    }

                })
                .setPositiveButton("შენახვა", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        Log.d(TAG, "SELECTED COLOR:" + selectedColor);
                        topBackgroundColor = selectedColor;
                        BACKGROUND_GRADIENT.setColors(new int[]{topBackgroundColor, bottomBackgroundColor});
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateDisplay();
                            }
                        });
                        Toast.makeText(MainActivity.this, "ფერი არჩეულია", Toast.LENGTH_SHORT).show();

                    }
                })
                .setNegativeButton("გაუქმება", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).noSliders().build().show();


    }


    @OnClick(R.id.daylyButton)
    public void startDailyActivity(View view){
        Intent intent = new Intent(this, DailyForecastActivity.class);
        try{
            intent.putExtra(DAILY_FORECAST, mForecast.getDailyForecast());
            startActivity(intent);
        }catch (NullPointerException e){
            Toast.makeText(MainActivity.this, "კავშირი არ არის", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.hourlyButton)
    public void startHourlyActivity(View view){
        Intent intent = new Intent(this, HourlyForecastActivity.class);
        try {
            intent.putExtra(HOURLY_FORECAST, mForecast.getHourlyForecast());
            startActivity(intent);
        }catch (NullPointerException e){
            Toast.makeText(MainActivity.this, "კავშირი არ არის", Toast.LENGTH_SHORT).show();
        }
    }

}
