package com.bat.weatherapp.weather;

import android.graphics.Color;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by ted on 3/26/16.
 */
public class Current {
    private String mIcon;
    private long mTime;
    private double mTemperature;
    private double mHumidity;
    private double mPrecipChance;
    private String mTimeZone;
    private String summary;

    public String getTimeZone() {
        return mTimeZone;
    }

    public void setTimeZone(String timeZone) {
        mTimeZone = timeZone;
    }

    public String getIcon() {
        return mIcon;
    }

    public int getIconId(){
        return Forecast.getIconId(mIcon);
    }

    public void setIcon(String icon) {
        mIcon = icon;
    }

    public long getTime() {
        return mTime;
    }

    public String getFormattedTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("h:mm a");
        formatter.setTimeZone(TimeZone.getTimeZone(mTimeZone));
        Date dateTime = new Date(getTime() *1000);
        String timeString = formatter.format(dateTime);

        return timeString;
    }

    public void setTime(long time) {
        mTime = time;
    }

    public int getTemperature() {
        return (int)Math.round((mTemperature-32)*5/9);
    }

    public void setTemperature(double temperature) {
        mTemperature = temperature;
    }

    public double getHumidity() {
        return mHumidity;
    }

    public void setHumidity(double humidity) {
        mHumidity = humidity;
    }

    public int getPrecipChance() {
        return (int)Math.round(mPrecipChance * 100);
    }

    public void setPrecipChance(double precipChance) {
        mPrecipChance = precipChance;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }


    public int getMoodColor(){
        String color = "000000";
        String iconL = mIcon.toLowerCase();

        if(iconL.contains("cloudy") ||
                 iconL.contains("rain") ||
                 iconL.contains("wind") ||
                 iconL.contains("fog")){
            color = "ffababab";
        }else if(iconL.contains("sunny")){
            color = "ffffea00";
        }else if(iconL.contains("snow")){
            color = "ffffff";
        }else if(iconL.contains("degree")){
            color = "ffef8a25";
        }


        if(getTemperature() <= 0){
            color = "007fff";
        }


        return Color.parseColor("#"+color);
    }
}
