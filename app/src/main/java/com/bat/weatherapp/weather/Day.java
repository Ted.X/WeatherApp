package com.bat.weatherapp.weather;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by ted on 3/30/16.
 */
public class Day implements Parcelable{
    private long mTime;
    private double mTemperatureMax;
    private String mSummary;
    private String mIcon;
    private String mTimezone;


    public Day(){}


    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        mTime = time;
    }

    public String getSummary() {
        return mSummary;
    }

    public void setSummary(String summary) {
        mSummary = summary;
    }

    public int  getTemperatureMax() {
        return (int)Math.round((mTemperatureMax-32)*5/9);
    }

    public void setTemperatureMax(double temperatureMax) {
        mTemperatureMax = temperatureMax;
    }
    public String getIcon() {
        return mIcon;
    }

    public int getIconId() {
        return Forecast.getIconId(mIcon);
    }

    public void setIcon(String icon) {
        mIcon = icon;
    }

    public String getTimezone() {
        return mTimezone;
    }

    public void setTimezone(String timezone) {
        mTimezone = timezone;
    }

    public String getDayOfTheWeek(){
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE");
        formatter.setTimeZone(TimeZone.getTimeZone(mTimezone));

        Date datetime = new Date(mTime*1000);
        String DayOfTheWeek = formatter.format(datetime);
        String nameInGeorgian = "-------";
        switch (DayOfTheWeek){
            case "Monday":
                nameInGeorgian = "ორშაბათი";
                break;
            case "Tuesday":
                nameInGeorgian = "სამშაბათი";
                break;
            case "Wednesday":
                nameInGeorgian = "ოთხშაბათი";
                break;
            case "Thursday":
                nameInGeorgian = "ხუთშაბათი";
                break;
            case "Friday":
                nameInGeorgian = "პარასკევი";
                break;
            case "Saturday":
                nameInGeorgian = "შაბათი";
                break;
            case "Sunday":
                nameInGeorgian = "კვირა";
                break;
        }

        return nameInGeorgian;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mTime);
        dest.writeString(mSummary);
        dest.writeDouble(mTemperatureMax);
        dest.writeString(mIcon);
        dest.writeString(mTimezone);
    }

    private Day(Parcel in){
        mTime = in.readLong();
        mSummary = in.readString();
        mTemperatureMax = in.readDouble();
        mIcon = in.readString();
        mTimezone = in.readString();
    }

    public static final Creator<Day> CREATOR = new Creator<Day>() {
        @Override
        public Day createFromParcel(Parcel source) {
            return new Day(source);
        }

        @Override
        public Day[] newArray(int size) {
            return new Day[size];
        }
    };
}
